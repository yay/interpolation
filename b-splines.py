# Theory:
# https://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/B-spline/bspline-basis.html

# %%
import numpy
import random
import pylab as plt
from plot import plot_xy


def b_spline(i, p, u, ui):
    '''
    i - knot span index
    p - degree of B-spline basis function
    u - parameter, u[i] <= u < u[i+p+1]
    ui - knot vector, len(ui) = p + 2
    '''
    if p == 0:
        return 1 if ui[i] <= u < ui[i+1] else 0
    else:
        return (u - ui[i]) / (ui[i+p] - ui[i]) * b_spline(i, p-1, u, ui) \
             + (ui[i+p+1] - u) / (ui[i+p+1] - ui[i+1]) * b_spline(i+1, p-1, u, ui)


ui = [0, 1, 2, 3, 4, 5]

# %%
u_values = numpy.linspace(ui[0], ui[2], 21)
y_values = []
for u in u_values:
    y_values.append(b_spline(0, 1, u, ui))
plt.plot(u_values, y_values, 'b')

u_values = numpy.linspace(ui[1], ui[3], 21)
y_values = []
for u in u_values:
    y_values.append(b_spline(1, 1, u, ui))
plt.plot(u_values, y_values, 'g')

plt.grid()
plt.show()

# %%
# Render N_0,2(u)
u_values = numpy.linspace(min(ui), max(ui), (len(ui) - 1) * 20 + 1)
y_values = []
for u in u_values:
    y_values.append(b_spline(0, 2, u, ui))

plt.plot(u_values, y_values)
plt.grid()
plt.show()

# %%
u_values = numpy.linspace(min(ui), max(ui), (len(ui) - 1) * 20 + 1)
y_values = []
for u in u_values:
    y_values.append(b_spline(0, 4, u, ui))

plt.plot(u_values, y_values)
plt.grid()
plt.show()


# %%
def b_curve_uniform(p, points):
    result = []
    n = len(points)
    # The size of the knot vector is going to  be n + p + 1
    # where p is the order of the polynomial.
    knot_count = n + p + 1
    ui = numpy.linspace(0, 1, knot_count)
    # the curve goes from first + 1 to last - 1 knot
    # for a total of (knot_count - 3) intervals, with 20 segments per interval
    u_values = numpy.linspace(ui[1], ui[-2], (knot_count - 3) * 20 + 1)

    for u in u_values:
        sum = 0
        for i in range(0, n):
            sum += b_spline(i, p, u, ui) * points[i]
        result.append(sum)

    return result


def random_points(n, min_y=0, max_y=10):
    points = []
    for i in range(0, n):
        points.append([i, random.randint(min_y, max_y)])
    return points


# points = [[1, 8], [2, 4], [3, 7], [4, 6], [5, 3]]
# points = random_points(10)
points = [[0, 150],
          [1, 300],
          [2, 150],
          [3, 0],
          [12, 150],
          [13, 150],
          [14, 300],
          [15, 150],
          [17, 15],
          [18, 0],
          [19, 300],
          [20, 300],
          [22, 300],
          [23, 0]]


def b_curve_uniform_plot(points, order=2):
    n = len(points)
    xx, yy = zip(*points)

    # knots = numpy.linspace(0, 1, n * 20 + 1)

    # x_values = []
    # y_values = []

    # for u in knots:
    #     x_values.append(b_curve_uniform(order, u, xx))
    #     y_values.append(b_curve_uniform(order, u, yy))

    x_values = b_curve_uniform(order, xx)
    y_values = b_curve_uniform(order, yy)

    plot_xy(x_values, y_values, points)

b_curve_uniform_plot(points, 2)
b_curve_uniform_plot(points, 3)
b_curve_uniform_plot(points, 4)
b_curve_uniform_plot(points, 5)

