# %%
from sympy import *
from sympy.matrices import Matrix
from sympy.abc import theta

import numpy as np

import math

init_printing(use_latex='mathjax')

# %%
I = Matrix(
    [[1, 0, 0, 0],
     [0, 1, 0, 0],
     [0, 0, 1, 0],
     [0, 0, 0, 1]]
)
I

# %%
c11, c12, c13, c14 = symbols('c_11 c_12 c_13 c_14')
c21, c22, c23, c24 = symbols('c_21 c_22 c_23 c_24')
c31, c32, c33, c34 = symbols('c_31 c_32 c_33 c_34')
c41, c42, c43, c44 = symbols('c_41 c_42 c_43 c_44')

# (c11, c12, c13, c14,
#  c21, c22, c23, c24,
#  c31, c32, c33, c34,
#  c41, c42, c43, c44) =\
#  symbols(' '.join([f'c_{i+1}{j+1}' for i in range(4) for j in range(4)]))


M = Matrix(
    [[0, 0, 0, 1],
     [1, 1, 1, 1],
     [0, 0, 1, 0],
     [3, 2, 1, 0]]
)

C = Matrix(
    [[c11, c12, c13, c14],
     [c21, c22, c23, c24],
     [c31, c32, c33, c34],
     [c41, c42, c43, c44]]
)

(M @ C)

# %%

'''
Solving this system of 16 variables:

     M               C                 I
[ 0 0 0 1 ] [ c11 c12 c13 c14 ]   [ 1 0 0 0 ]
[ 1 1 1 1 ] [ c21 c22 c23 c24 ] = [ 0 1 0 0 ]
[ 0 0 1 0 ] [ c31 c32 c33 c34 ]   [ 0 0 1 0 ]
[ 3 2 1 0 ] [ c41 c42 c43 c44 ]   [ 0 0 0 1 ]

'''

S = solve(M * C - I)
S

# %%
repr(S)
