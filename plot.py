import pylab as plt

# from plot import plot_xy


def plot_xy(x, y, points=None):
    '''
    x, y - interpolated points at arbitrary interval(s)
    points - original points
    The interpolated points are used to render the curve.
    The original points are used to render the markers.
    '''
    plt.plot(x, y)

    if points:
        cx, cy = zip(*points)
        plt.plot(cx, cy, 'or')  # 'o' is for circle marker, 'r' is for red color

    plt.grid()
    plt.show()
