# %%
import numpy
import math

numpy.linspace(0, 2, 21)

# %%
numpy.arange(0, 2, 0.1)

# %%
numpy.linspace(0, 1, 6)

# %%
a = [1, 2, 3, 4, 5]
print(a[0])
print(a[1])
print(a[-1])
print(a[-2])

# %%
5**3

# %%
pow(5, 3)

# %%
# a = numpy.array([[2, 3, 4]])
# print(2 * a)
# print(numpy.exp(6))
# print(403.428793493 ** (1/math.e))
# print(numpy.exp(2 * a))
# print(5 ** 1000)
# print(5.0 ** 1000)
d = numpy.array([[1089, 1093]])
e = numpy.array([[1000, 4443]])
answer = numpy.exp(-3 * d)
print(answer)
answer1 = numpy.exp(-3 * e)
print(answer1)
res = answer.sum()/answer1.sum()


