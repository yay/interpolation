On the unit interval $(0,1)$, given a starting point $p_0$ at $t=0$ and an ending point $p_1$ at $t=1$ with starting tangent $m_0$ at $t=0$ and ending tangent $m_1$ at $t=1$, the polynomial can be defined by

$p(t) = (2t^{3}-3t^{2}+1)p_0 + (t^{3}-2t^{2}+t)m_0 + (-2t^{3}+3t^{2})m_1 + (t^{3}-t^{2})m_1$

where t ∈ [0, 1].

<img src="hermit.svg" width=400>

$$
p(t) = \begin{bmatrix}t^3 & t^2 & t & 1\end{bmatrix} \cdot
\begin{bmatrix}
2 & -2 & 1 & 1\\
-3 & 3 & -2 & -1\\
0 & 0 & 1 & 0\\
1 & 0 & 0 & 0\\
\end{bmatrix} \cdot
\begin{bmatrix}
p_0\\
p_1\\
m_0\\
m_1\\
\end{bmatrix}
$$
