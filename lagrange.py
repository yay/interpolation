# Theory: https://www.youtube.com/watch?v=vAgKE5wvR4Y

# %%
import numpy
import pylab as plt


def plot_xy(x, y, points=None):
    '''
    x, y - interpolated points at arbitrary interval(s)
    points - original points
    The interpolated points are used to render the curve.
    The original points are used to render the markers.
    '''
    plt.plot(x, y)

    if points:
        cx, cy = zip(*points)
        plt.plot(cx, cy, 'or')  # 'o' is for circle marker, 'r' is for red color

    plt.grid()
    plt.show()


def lagrange_first_order(x, x1, x2, y1, y2):
    return (x - x2) / (x1 - x2) * y1 \
         + (x - x1) / (x2 - x1) * y2


def lagrange_second_order(x, x1, x2, x3, y1, y2, y3):
    return ((x - x2) * (x - x3)) / ((x1 - x2) * (x1 - x3)) * y1 \
         + ((x - x1) * (x - x3)) / ((x2 - x1) * (x2 - x3)) * y2 \
         + ((x - x1) * (x - x2)) / ((x3 - x1) * (x3 - x2)) * y3


def lagrange_auto_order(x, points):
    n = len(points)
    if n < 2:
        raise Exception('Need at least two points.')

    F = 0
    for i in range(0, n):
        L = 1

        for j in range(0, n):
            if j != i:
                # first index -> index of the point
                # second index -> 0 for x, 1 for y
                L *= (x - points[j][0]) / (points[i][0] - points[j][0])

        F += L * points[i][1]

    return F

# Task:
# given the points below, approximate the value of the function
# at point (3.8, ?)
points = [[1, 8], [2, 4], [3, 7], [4, 6], [5, 3]]

# For second order lagrange polynomial, choose 3 closest points to x = 3.8:
# x = 3, 4, 5
control_points = points[2:]
cx, cy = zip(*control_points)
# the results below should be the same
print("Second order:", lagrange_second_order(3.8, *cx, *cy))
print("Auto order (second):", lagrange_auto_order(3.8, control_points))

control_points = points[1:]  # now use 4 points for a third order polynomial
cx, cy = zip(*control_points)
# as the order increases the interpolated value probably moves closer
# to its true answer
print("Auto order (third):", lagrange_auto_order(3.8, control_points))


# %%
def lagrange_first_plot(points, nsteps=11):
    n = len(points)
    x, y = zip(*points)
    xx = []
    yy = []
    # for every pair of points
    for i in range(0, n-1):
        # split each interval into nsteps
        xis = numpy.linspace(x[i], x[i+1], nsteps)
        xx.extend(xis)
        # and calculate the value of the polynomial for every step
        for xi in xis:
            yi = lagrange_first_order(xi, x[i], x[i+1], y[i], y[i+1])
            yy.append(yi)

    plot_xy(xx, yy, points)

lagrange_first_plot(points)


# %%
def lagrange_second_multiplot(points, nsteps=21):
    n = len(points)
    x, y = zip(*points)
    for i in range(0, n-2):
        xx = numpy.linspace(x[i], x[i+2], nsteps)
        yy = []
        for xi in xx:
            yi = lagrange_second_order(xi, x[i], x[i+1], x[i+2], y[i], y[i+1], y[i+2])
            yy.append(yi)

        plot_xy(xx, yy, points)

lagrange_second_multiplot(points)


# %%
def lagrange_auto_plot(points, nsteps=101):
    x, y = zip(*points)
    min_x = min(x)
    max_x = max(x)
    xx = numpy.linspace(min_x, max_x, nsteps)
    yy = []
    for i in range(0, len(xx)):
        yy.append(lagrange_auto_order(xx[i], points))

    plot_xy(xx, yy, points)

lagrange_auto_plot(points)
