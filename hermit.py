# Cubic Hermite Spline (interpolator)

import numpy
import pylab as plt

p0 = [0, 0]
p1 = [2, 0]
m0 = 2   # slope at point p0
m1 = -8  # slope at point p1

# m0 = -2
# m1 = -8


def tangent(m, x, b):
    return m*x + b

dx = 0.3
t0x0 = p0[0] - dx
t0x1 = p0[0] + dx
b = p0[1] - m0 * p0[0]  # y = mx + b => b = y - mx
t0y0 = tangent(m0, t0x0, b)
t0y1 = tangent(m0, t0x1, b)
t1x0 = p1[0] - dx
t1x1 = p1[0] + dx
b = p1[1] - m1 * p1[0]
t1y0 = tangent(m1, t1x0, b)
t1y1 = tangent(m1, t1x1, b)


def hermit_third_order(p0, p1, m0, m1, t):
    return (2*t**3 - 3*t**2 + 1) * p0 \
         + (t**3 - 2*t**2 + t) * m0 \
         + (-2*t**3 + 3*t**2) * p1 \
         + (t**3 - t**2) * m1

step_count = 50
steps = numpy.linspace(0, 1, step_count)

xx = []
yy = []

for i in range(0, step_count):
    step = steps[i]
    x = hermit_third_order(p0[0], p1[0], 1, 1, step)
    y = hermit_third_order(p0[1], p1[1], m0, m1, step)
    xx.append(x)
    yy.append(y)

plt.plot(xx, yy, 'b',
         [t0x0, t0x1], [t0y0, t0y1], 'r',
         [t1x0, t1x1], [t1y0, t1y1], 'r')
plt.grid()
plt.show()
