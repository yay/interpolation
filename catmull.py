# %%
import numpy
import pylab as plt
from plot import plot_xy


def catmull_rom___(points, alpha=0, segments=20):
    '''
    alpha - parametrization
    alpha = 0 (default) - uniform
    alpha = 0.5 - centripetal
    alpha = 1 - chordal

    points - points of the segment
    4 points expected, i.e. cubic Catmull-Rom curve

    segments - number of subdivisions
    '''
    n = len(points)
    if n != 4:
        raise Exception(f'4 points expected, found {n}')

    P0 = points[0]
    P1 = points[1]
    P2 = points[2]
    P3 = points[3]

    def sign(x): return 1 if x > 0 else -1 if x < 0 else 0

    t0 = 0
    t1 = t0 + sign(P1 - P0) * abs(P1 - P0) ** alpha
    t2 = t1 + sign(P2 - P1) * abs(P2 - P1) ** alpha
    t3 = t2 + sign(P3 - P2) * abs(P3 - P2) ** alpha

    space = numpy.linspace(t1, t2, segments + 1)

    result = []

    for t in space:

        d = t1 - t0
        L01 = 0 if d == 0 else P0 * (t1 - t) / d + P1 * (t - t0) / d
        d = t2 - t1
        L12 = 0 if d == 0 else P1 * (t2 - t) / d + P2 * (t - t1) / d
        d = t3 - t2
        L23 = 0 if d == 0 else P2 * (t3 - t) / d + P3 * (t - t2) / d

        d = t2 - t0
        L012 = 0 if d == 0 else L01 * (t2 - t) / d + L12 * (t - t0) / d
        d = t3 - t1
        L123 = 0 if d == 0 else L12 * (t3 - t) / d + L23 * (t - t1) / d

        d = t2 - t1
        C12 = 0 if d == 0 else L012 * (t2 - t) / d + L123 * (t - t1) / d

        result.append(C12)

    return result


def catmull_rom(points, alpha=0, segments=20):
    '''
    alpha - parametrization
    alpha = 0 (default) - uniform
    alpha = 0.5 - centripetal
    alpha = 1 - chordal

    points - points of the segment
    4 points expected, i.e. cubic Catmull-Rom curve

    segments - number of subdivisions
    '''
    n = len(points)
    if n != 4:
        raise Exception(f'4 points expected, found {n}')

    P0 = points[0]
    P1 = points[1]
    P2 = points[2]
    P3 = points[3]

    t0 = 0
    t1 = t0 + (P1 - P0) ** alpha
    t2 = t1 + (P2 - P1) ** alpha
    t3 = t2 + (P3 - P2) ** alpha

    # print(t0, t1, t2, t3)

    space = numpy.linspace(t1, t2, segments + 1)

    result = []

    for t in space:

        L01 = P0 * (t1 - t) / (t1 - t0) + P1 * (t - t0) / (t1 - t0)
        L12 = P1 * (t2 - t) / (t2 - t1) + P2 * (t - t1) / (t2 - t1)
        L23 = P2 * (t3 - t) / (t3 - t2) + P3 * (t - t2) / (t3 - t2)

        L012 = L01 * (t2 - t) / (t2 - t0) + L12 * (t - t0) / (t2 - t0)
        L123 = L12 * (t3 - t) / (t3 - t1) + L23 * (t - t1) / (t3 - t1)

        C12 = L012 * (t2 - t) / (t2 - t1) + L123 * (t - t1) / (t2 - t1)

        result.append(C12)

    return result


points = [[1, 3], [2, 6], [4, 4], [4, 7]]


def plot_curve(points):
    x_points, y_points = zip(*points)

    xx = catmull_rom(x_points)
    yy = catmull_rom(y_points)

    plt.xlim(0, 5)
    plt.ylim(2, 8)
    plt.autoscale(False)
    plot_xy(xx, yy, points)
    # plt.plot(xx, yy)
    # plt.grid()
    # plt.show()


# plot_curve(points)
# plot_curve([[1, 3], [1, 3], [2, 6], [4, 4]])
# plot_curve([[2, 6], [4, 4], [4, 7], [4, 7]])


def interpolate_segment(points, out_x, out_y):
    n = len(points)
    if n != 4:
        raise Exception(f'4 points expected, found {n}')

    x_points, y_points = zip(*points)

    xx = catmull_rom(x_points, 0)
    yy = catmull_rom(y_points, 0)

    out_x.extend(xx)
    out_y.extend(yy)

    return xx, yy

xx = []
yy = []
interpolate_segment([[1, 3], [1, 3], [2, 6], [4, 4]], xx, yy)
interpolate_segment(points, xx, yy)
interpolate_segment([[2, 6], [4, 4], [4, 7], [4, 7]], xx, yy)
# print([list(t) for t in zip(xx, yy)])

plt.xlim(0, 5)
plt.ylim(2, 8)
plt.autoscale(False)
plot_xy(xx, yy, points)
