# %%
import numpy as np
import random
import pylab as plt
from plot import plot_xy

cubic_coef = np.array(
    [[1, 0, 0, 0],
     [-3, 3, 0, 0],
     [3, -6, 3, 0],
     [-1, 3, -3, 1]]
)


# Cardinal (Catmull-Rom) spline with tension `t`.
def cardinalToBezier(points, t=1):

    P1 = points[0]
    P2 = points[1]
    P3 = points[2]
    P4 = points[3]

    return [
        P2,
        P2 + (P3 - P1) / 6 * t,
        P3 - (P4 - P2) / 6 * t,
        P3
    ]


def bezier_cubic(points, t):
    points = cardinalToBezier(points)
    points = np.asarray(points)
    params = np.array([1, t, t**2, t**3])
    return params @ cubic_coef @ points


def random_points(n, min_y=0, max_y=10):
    points = []
    for i in range(0, n):
        points.append([i, random.randint(min_y, max_y)])
    return points


# points = random_points(4)
points = [[1, 3], [2, 6], [4, 4], [4, 7]]


def plot_cubic(points):
    x_points, y_points = zip(*points)
    t_space = np.linspace(0, 1, 51)
    xx = []
    yy = []
    for t in t_space:
        xx.append(bezier_cubic(x_points, t))
        yy.append(bezier_cubic(y_points, t))

    # plt.xlim(-10, 110)
    # plt.ylim(-10, 10)
    # plt.autoscale(False)
    plot_xy(xx, yy, points)


plot_cubic([[1, 3], [1, 3], [2, 6], [4, 4]])
plot_cubic(points)
plot_cubic([[2, 6], [4, 4], [4, 7], [4, 7]])
# plot_cubic([[0, 0], [0, 0], [1, 1], [2, 0]])
# plot_cubic([[0, 0], [1, 1], [2, 0], [2, 0]])

# plot_cubic([[0, 0], [48, 0], [52, 0], [100, 0]])
# plot_cubic([[0, 0], [48, 1], [52, -1], [100, 0]])
# plot_cubic([[0, 0], [50, 1], [50, -1], [100, 0]])
# plot_cubic([[0, 0], [51, 1], [49, -1], [100, 0]])



